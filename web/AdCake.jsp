<%-- 
    Document   : AdCake
    Created on : Jul 9, 2023, 11:20:16 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/board.css">
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        <c:if test="${!empty user && user.role}">
            <!-------------------------------------------------------------->
        <section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 text-right mb-5 choice">
                                    <a href="AdCakeAdd.jsp" style="padding: 10px; background-color: #ffcc66; margin-left: 900px">Add new cake</a>
				</div>
                                <div class="col-md-12 text-left mb-5 choice">
                                    <a href="AdCake" style="padding: 10px; background-color: #cccccc;">All Cake</a>
                                    <a href="AdCakeListRetype?retype=2" style="padding: 10px; background-color: #cccccc;">Best Seller</a>
                                    <a href="AdCakeListRetype?retype=1" style="padding: 10px; background-color: #cccccc;">New product</a>
                                    <a href="AdCakeListRetype?retype=3" style="padding: 10px; background-color: #cccccc;">Banner</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table table-striped">
						  <thead>
						    <tr>
						      <th>ID</th>
                                                      <th>Image</th>
                                                      <th style="width: 15%">Name</th>
                                                      <th>Price</th>
                                                      <th style="width: 20%">Infor</th>
                                                      <th style="width: 20%">Type</th>
                                                      <th style="width: 60%">Service</th>    
						    </tr>
						  </thead>
						  <tbody>
                                                      <c:forEach items="${list}" var="li">
                                                        <tr>
                                                          <td>${li.id}</td>
                                                          <td><img src="Pic/${li.image}" style="width: 150px"></td>
                                                          <td><a href="CakeDetail?id=${li.id}" target="_blank">${li.name}</a></td>
                                                          <td>${li.price}00</td>
                                                          <td>${li.infor}</td>
                                                          <td style="color: #993300">${li.typename}</td>
                                                          <td>
                                                              <c:choose>
                                                                  <c:when test="${li.isBanner}">
                                                                        <a href="AdCakeChangeRetype?id=${li.id}&method=remove&typech=3" class="btn btn-primary" style="margin-bottom: 10px;">Delete from Banner</a>
                                                                  </c:when>
                                                                  <c:otherwise>
                                                                      <a href="AdCakeChangeRetype?id=${li.id}&method=add&typech=3" class="btn btn-info" style="margin-bottom: 10px;">Add to Banner</a>
                                                                  </c:otherwise>
                                                              </c:choose>
                                                                      
                                                               <c:choose>
                                                                  <c:when test="${li.isBestseller}">
                                                                        <a href="AdCakeChangeRetype?id=${li.id}&method=remove&typech=2" class="btn btn-primary" style="margin-bottom: 10px;">Delete from Best Seller</a>
                                                                  </c:when>
                                                                  <c:otherwise>
                                                                      <a href="AdCakeChangeRetype?id=${li.id}&method=add&typech=2" class="btn btn-info" style="margin-bottom: 10px;">Add to Best Seller</a>
                                                                  </c:otherwise>
                                                              </c:choose>
                                                                      
                                                              <c:choose>
                                                                  <c:when test="${li.isNew}">
                                                                        <a href="AdCakeChangeRetype?id=${li.id}&method=remove&typech=1" class="btn btn-primary" style="margin-bottom: 10px;">Delete from New Product</a>
                                                                  </c:when>
                                                                  <c:otherwise>
                                                                      <a href="AdCakeChangeRetype?id=${li.id}&method=add&typech=1" class="btn btn-info" style="margin-bottom: 10px;">Add to New Product</a>
                                                                  </c:otherwise>
                                                              </c:choose>
                                                                <a href="AdCakeDelete?id=${li.id}" class="btn btn-danger" style="margin-bottom: 10px;">Delete</a>  
                                                          </td>
                                                        </tr>
                                                      </c:forEach>
						    
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
        </c:if>
        
    </body>
</html>
