<%-- 
    Document   : AdUser
    Created on : Jul 8, 2023, 8:58:13 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/board.css">
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        <c:if test="${!empty user && user.role}">
            <!-------------------------------------------------------------->
        <section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
                            <c:if test="${!empty msg}">
                        <div class="alert alert-danger ">
                        <strong>${msg}</strong>
                        </div>
                        </c:if>
				<div class="col-md-12 text-left mb-5 choice">
                                    <a href="AdUserAdd.jsp" style="padding: 10px; background-color: #cccccc;">Add new user</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table table-striped">
						  <thead>
						    <tr>
						      <th>Phone</th>
                                                      <th>Full name</th>
						      <th>Username</th>
                                                      <th>Address</th>
                                                      <th style="width: 40%">Service</th>                                                      
						    </tr>
						  </thead>
						  <tbody>
                                                      <c:forEach items="${list}" var="li">
                                                        <tr>
                                                          <td>${li.phone}</td>
                                                          <td>${li.fullname}</td>
                                                          <td>${li.username}</td>
                                                          <td>${li.address}</td>
                                                          <td>
                                                                <a href="AdUserFix.jsp?phone=${li.phone}&username=${li.username}" class="btn btn-info" style="margin-bottom: 10px;">Change password</a>
                                                                <a href="AdUserFixInfor.jsp?phone=${li.phone}&username=${li.username}&fullname=${li.fullname}" class="btn btn-success" style="margin-bottom: 10px;">Change information</a>
                                                                <c:if test="${!li.role}"><a href="AdUserDelete?phone=${li.phone}" class="btn btn-danger" style="margin-bottom: 10px;">Delete</a></c:if>                       
                                                          </td>
                                                        </tr>
                                                      </c:forEach>
						    
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

        <!-------------------------------------------------------------------->
        </c:if>
        
        
    </body>
</html>
