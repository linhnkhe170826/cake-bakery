<%-- 
    Document   : All Cake
    Created on : Jul 5, 2023, 2:38:29 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/css/theme.css" rel="stylesheet" />
        <script type="text/javascript" src="assets/js/Cart.js"></script>
        <script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
    </head>
    <body>
        <main class="main" id="top">
         <jsp:include page="Header.jsp" />
        /<!-- header -->
        <section class="padding"></section>   
        <section class="py-0">
        <div class="container">
          <div class="Find">
              <label>
                  <form action="FindCake" method="POST">
                      <div class="col-12">
                    Find By:
                    <select class="input-select" name="num" >
                            <option value="0">Choses</option>
                            <option value="10">Price: High to Low</option>
                            <option value="11">Price: Low to High</option>
			</select>
                    
                    <select class="input-select" name="numb">
                        <option value="0">Choses</option>
                        <c:forEach items="${listt}" var="type">
                            <option value="${type.typeid}">${type.typename}</option>
                        </c:forEach>
                        </select>
                        <input type="text" class="input" placeholder="Tên bánh" name="search">
                        <button>Search</button>
                    </div>
                    </form>
                    
		</label>
          </div>
            <div class="col-12" id="carouselBestDeals" data-bs-touch="false" data-bs-interval="false">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-bs-interval="10000">
                    <div class="row h-100 align-items-center g-2">
                      <c:forEach items="${list}" var="s">
                        <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                            <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="Pic/${s.image}" alt="..." />
                                <div class="card-body ps-0 bg-200">
                                <h5 class="fw-bold text-1000 text-truncate">${s.name}</h5>
                                <div class="fw-bold"><span class="text-primary">${s.price}00</span></div>
                                </div>
                                <div class="row">
                                <div class="buttonsub col-sm-3 col-md-12">
                                    <div class="add-to-cart">
					<button class="add-to-cart-btn"><a href="${pageContext.request.contextPath}/CakeDetail?id=${s.id}">View detail</a></button>
                                        <button class="add-to-cart-btn"><a onclick="buy(${s.id})">Add to cart</a></button>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                                </div>
                        </div>       
                        </c:forEach>
                      </div>
                    </div>
                    </div>
                  </div>
                
          </div>
          </div>
   
        <!-- end of .container-->

        <jsp:include page="Footter.html" />
        
      </section>
        </main>
    </body>
</html>
