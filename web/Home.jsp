<%-- 
    Document   : Home
    Created on : Jul 4, 2023, 9:49:30 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="assets/css/theme.css" rel="stylesheet" />
    <script type="text/javascript" src="assets/js/Cart.js"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
    </head>
    <body>
        <jsp:include page="Header.jsp" />
        
        
        <main class="main" id="top">
            
      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-11 bg-light-gradient border-bottom border-white border-5">
        <div class="bg-holder overlay overlay-light" style="background-image:url(Pic/Topbanner.jpg);background-size:cover;">
        
        <!--/.bg-holder-->

        <div class="container">
          <div class="row flex-center">
            <div class="col-12 mb-10">
              <div class="d-flex align-items-center flex-column">
                <h1 class="fw-normal1"> May your day be filled with sweetness</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>
      <section class="py-0" id="header" style="margin-top: -23rem !important;">

        <div class="container">
          <div class="row g-0">
              <c:forEach items="${listbn}" var="bn">
            <div class="col-md-6">
              <div class="card card-span h-100 text-white"> <img class="img-fluid" src="Pic/${bn.image}" width="900"/>
                <div class="card-img-overlay d-flex flex-column-reverse"> <a class="btn btn-lg btn-light" onclick="buy(${bn.id})">Mua ngay</a></div>
              </div>
            </div>
              </c:forEach>
          </div>
        </div>
        <!-- end of .container-->

      </section>

      <!-- <section> close ============================-->
      <!-- ============================================-->
      
      <section class="py-0">

        <div class="container">
          <div class="row h-100">
              <div class="col-lg-7 mx-auto text-center mt-7 mb-5">
              <h5 class="fw-bold fs-3 fs-lg-5 lh-sm">New Product</h5>
            </div>
              
            <div class="col-12" id="carouselBestDeals" data-bs-touch="false" data-bs-interval="false">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-bs-interval="10000">
                    <div class="row h-100 align-items-center g-2">
                      <c:forEach items="${listn}" var="s">
                        <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                            <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="Pic/${s.image}" alt="..." />
                                <div class="card-body ps-0 bg-200">
                                <h5 class="fw-bold text-1000 text-truncate">${s.name}</h5>
                                <div class="fw-bold"><span class="text-primary">${s.price}00</span></div>
                                </div>
                                <div class="row">
                                <div class="buttonsub col-sm-3 col-md-12">
                                    <div class="add-to-cart">
					<button class="add-to-cart-btn"><a href="${pageContext.request.contextPath}/CakeDetail?id=${s.id}">View detail</a></button>
                                        <button class="add-to-cart-btn"><a onclick="buy(${s.id})">Add to cart</a></button>
                                    </div>
                                </div>
                          </div>
                            </div>
                        </div>
                        </c:forEach>
                      </div>
                    </div>
                    </div>
                  </div>
                
          </div>
          </div>
   
        <!-- end of .container-->

        
        
      </section>



      <section class="py-0">

        <div class="container">
          <div class="row h-100">
              <div class="col-lg-7 mx-auto text-center mt-7 mb-5">
              <h5 class="fw-bold fs-3 fs-lg-5 lh-sm">Best Seller</h5>
            </div>
              
            <div class="col-12" id="carouselBestDeals" data-bs-touch="false" data-bs-interval="false">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-bs-interval="10000">
                    <div class="row h-100 align-items-center g-2">
                      <c:forEach items="${listbs}" var="s">
                        <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                            <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="Pic/${s.image}" alt="..." />
                                <div class="card-body ps-0 bg-200">
                                <h5 class="fw-bold text-1000 text-truncate">${s.name}</h5>
                                <div class="fw-bold"><span class="text-primary">${s.price}00</span></div>
                                </div>
                                <div class="row">
                                <div class="buttonsub col-sm-3 col-md-12">
                                    <div class="add-to-cart">
					<button class="add-to-cart-btn"><a href="${pageContext.request.contextPath}/CakeDetail?id=${s.id}">View detail</a></button>
                                        <button class="add-to-cart-btn"><a onclick="buy(${s.id})">Add to cart</a></button>
                                    </div>
                                </div>
                          </div>
                            </div>
                        </div>
                        </c:forEach>
                      </div>
                    </div>
                    </div>
                  </div>
                
          </div>
          </div>
      </section>
      
      <section class="padding">
        </section>
      
      <section class="py-11">
        <div class="bg-holder overlay overlay-0" style="background-image:url(Pic/BotBanner.jpg);background-position:center;background-size:cover;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="carousel slide carousel-fade" id="carouseCta" data-bs-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active" data-bs-interval="10000">
                    <div class="row h-100 align-items-center g-2">
                      <div class="col-12">
                        <div class="text-light text-center py-2">
                          <h5 class="display-4 fw-normal text-400 fw-normal mb-4">visit our Bakery in</h5>
                          <h1 class="display-1 text-white fw-normal mb-8">Ha Noi</h1><a class="btn btn-lg text-light fs-1" href="https://www.google.com/maps/place/Panacota+Handmade+Bakery/@21.0189824,105.8179724,19.27z/data=!4m6!3m5!1s0x3135ab655d953907:0x770f8a4b8b2785ce!8m2!3d21.0189489!4d105.818412!16s%2Fg%2F11bw2hyxc3?entry=ttu" target="_blank" role="button">See Addresses
                            <svg class="bi bi-arrow-right-short" width="23" height="23" fill="currentColor" viewBox="0 0 16 16">
                              <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"> </path>
                            </svg></a>
                        </div>
                      </div>
                    </div>
                  </div>
                 </div>
              </div>  
                </div>
                 </div>
              </div>
      </section>
      <!<!-- Footer -->
      
      <jsp:include page="Footter.html" />
 


     
      <!-- =========================================bg-holder===-->

 


    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="vendors/@popperjs/popper.min.js"></script>
    <script src="vendors/bootstrap/bootstrap.min.js"></script>
    <script src="vendors/is/is.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
    <script src="vendors/feather-icons/feather.min.js"></script>
    <script>
      feather.replace();
    </script>
    <script src="assets/js/theme.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
  
        
            
    </body>
</html>
