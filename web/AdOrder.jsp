<%-- 
    Document   : AdOrder
    Created on : Jul 8, 2023, 8:58:13 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/board.css">
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        <c:if test="${!empty user && user.role}">
            <!-------------------------------------------------------------->
        <section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 text-left mb-5 choice">
                                    <a href="AdOrderList" style="padding: 10px; background-color: #cccccc;">All order</a>
                                    <a href="AdOrderListSt?status=1" style="padding: 10px; background-color: #cccccc;">Not pay</a>
                                    <a href="AdOrderListSt?status=2" style="padding: 10px; background-color: #cccccc;">Have payed</a>
                                    <a href="AdOrderListSt?status=3" style="padding: 10px; background-color: #cccccc;">Completed</a>
                                    <a href="AdOrderListSt?status=4" style="padding: 10px; background-color: #cccccc;">Done</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table table-striped">
						  <thead>
						    <tr>
						      <th>ID</th>
						      <th>Total</th>
                                                      <th>Buyer detail</th>
						      <th>Items</th>
                                                      <th>Buyer name</th>
                                                      <th>Status</th>
                                                      <th>Order date</th>
                                                      <th>Status</th>                                                      
						    </tr>
						  </thead>
						  <tbody>
                                                      <c:forEach items="${list}" var="li">
                                                        <tr>
                                                          <th scope="row">${li.id}</th>
                                                          <td>${li.total}00</td>
                                                          <td>
                                                              <p>${li.name}</p>
                                                              <p>${li.phone}</p>
                                                              <p>${li.address}</p>
                                                          </td>
                                                          <td>
                                                              <c:forEach items="${li.itemAL}" var="it">
                                                                  <p style="font-size: 13px; margin-bottom: -2px;">${it.cname} (${it.price}00) x${it.amount}</p>
                                                              </c:forEach>
                                                          </td>
                                                          <td>${li.name}</td>
                                                          <td>${li.date}</td>
                                                          <td>
                                                             <c:if test="${li.sattus==2}"><a style="color: black;">Have payed</a></c:if>
                                                             <c:if test="${li.sattus==3}"><a style="color:green;">Completed</a></c:if>
                                                             <c:if test="${li.sattus==1}"><a style="color: red;">Not payed</a></c:if> 
                                                          </td>
                                                          <td>
                                                          <c:if test="${li.sattus==1}"><a href="AdOrderChStatus?id=${li.id}&status=4" class="btn btn-dark" style="margin-bottom: 10px;">COD</a></c:if>
                                                          <c:if test="${li.sattus==2}"><a href="AdOrderChStatus?id=${li.id}&status=3" class="btn btn-info" style="margin-bottom: 10px;">Release</a></c:if>
                                                          <c:if test="${li.sattus==3}"><a href="AdOrderChStatus?id=${li.id}&status=4" class="btn btn-success" style="margin-bottom: 10px;">Completed</a></c:if>
                                                                                      <a href="AdOrderListDelete?id=${li.id}" class="btn btn-danger" style="margin-bottom: 10px;">Delete</a>
                                                          </td>
                                                        </tr>
                                                      </c:forEach>
						    
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

        <!-------------------------------------------------------------------->
        </c:if>
        
        
    </body>
</html>
