<%-- 
    Document   : UserCenter
    Created on : Jul 7, 2023, 12:53:19 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/css/login.css" rel="stylesheet" />
    </head>
    <body>
        <jsp:include page="Header.jsp" />
        /<!-- header -->
        <section class="padding"></section> 
        
            
            <div class="box" style="background-image:url(Pic/LoginBack.jpg)";>
                
                
                <div class="container">
                    <form action="ChangeInfor" method="POST">
                    <div class="col-lg-7 mx-auto text-center mb-6">
                    <div class="top">
                        <h5 class="fw-bold fs-3 fs-lg-5 lh-sm mb-3" style="color: #ffe5c5; padding-top: 20px;">User Center</h5>
                    </div>
                        <c:if test="${!empty msg}">
                            <div class="alert alert-success">
                            <strong>${msg}</strong>
                            </div>
                        </c:if>
                        <div style="color: #ffffff; font-size: 30px; padding-bottom: 10px;text-align: left;padding-left: 70px;">Change information</div>
                    
                        <p style="color: #feedd8; font-size: 20px; text-align: left; padding-left: 70px; margin-bottom: -3px; font-weight: 500; ">Full Name:</p>
                    <div class="input-field">
                        <input type="text" class="input" placeholder="Họ tên" name="Fullname" value="${user.fullname}" >
                        <i class='bx bx-lock-alt'></i>
                    </div>
                    
                        <p style="color: #feedd8; font-size: 20px; text-align: left; padding-left: 70px; margin-bottom: -3px; font-weight: 500; ">User name:</p>
                    <div class="input-field">
                        <input type="text" class="input" placeholder="Username" name="username" value="${user.username}" >
                        <i class='bx bx-user' ></i>
                    </div>
                        <p style="color: #feedd8; font-size: 20px; text-align: left; padding-left: 70px; margin-bottom: -3px; font-weight: 500; ">Address:</p>
                    <div class="input-field">
                        <input type="text" class="input" placeholder="Địa chỉ" name="Address" value="${user.address}">
                        <i class='bx bx-lock-alt'></i>
                    </div>
                        
                        <button class="add-to-cart-btn"><input type="submit" value="SAVE"></button>
                    </div>
                    </form>
                        
                        <!-- ----------------------------------------------------------------------- -->         
                     <form action="ChangePassword" method="POST">
                    <div class="col-lg-7 mx-auto text-center mb-6">
                        <c:if test="${!empty fmsg}">
                            <div class="alert alert-danger">
                            <strong>${fmsg}</strong>
                            </div>
                        </c:if>
                        <div style="color: #ffffff; font-size: 30px; padding-bottom: 10px;text-align: left;padding-left: 70px; padding-top: 10px;">Change password</div>
                        <p style="color: #feedd8; font-size: 20px; text-align: left; padding-left: 70px; margin-bottom: -3px; font-weight: 500; ">Password:</p>
                        <div class="input-field">
                        <input type="Password" class="input" placeholder="Mật khẩu cũ" name="Password" >
                        <i class='bx bx-lock-alt'></i>
                    </div>

                        <p style="color: #feedd8; font-size: 20px; text-align: left; padding-left: 70px; margin-bottom: -3px; font-weight: 500; ">Comfirm password:</p>
                    <div class="input-field">
                        <input type="Password" class="input" placeholder="Mật khẩu mới" name="NPassword" >
                        <i class='bx bx-lock-alt'></i>
                    </div>

                        <button class="add-to-cart-btn"><input type="submit" value="SAVE"></button>
                        
                </div>
                    </form>
            </div>
                        
            </div>
        
        <jsp:include page="Footter.html" />
    </body>
</html>
