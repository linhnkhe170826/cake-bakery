<%-- 
    Document   : Cart
    Created on : Jul 7, 2023, 4:29:40 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/cart.css" rel="stylesheet" />
        <script type="text/javascript" src="assets/js/Cart.js"></script>
        <script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
    </head>
    <body>
        <jsp:include page="Header.jsp" />
        <!-- ---------------------------------------------------------- -->
        <section class="padding"></section>   
        <section class="padding"></section>   
        
            <div class="card">
            <div class="row">
                <div class="col-md-8 cart">
                    <div class="title">
                        <div class="row">
                            <c:if test="${!empty msg}">
                        <div class="alert alert-danger ">
                        <strong>${msg}</strong>
                        </div>
                            </c:if>
                            <div class="col"><h4><b>My Shopping Cart</b></h4></div>
                        </div>
                    </div>
                    <c:forEach items="${order.itemMap}" var="item">
                    <div class="row border-top border-bottom">
                        <div class="row main align-items-center">
                            <div class="col-2"><a href="${pageContext.request.contextPath}/CakeDetail?id=${item.value.cake.id}"><img class="img-fluid" src="Pic/${item.value.cake.image}"></a></div>
                            <div class="col">
                                <div class="row"><a href="${pageContext.request.contextPath}/CakeDetail?id=${item.value.cake.id}">${item.value.cake.name}</a></div>
                            </div>
                            <div class="col">
                                <a href="javascript:less(${item.value.cake.id})">-</a><a class="border" >${item.value.amount}</a><a href="javascript:buy(${item.value.cake.id})">+</a>
                            </div>
                            <div class="col">${item.value.price}00   <a class="close" href="javascript:Delete(${item.value.cake.id})">&#10005;</a></div> <!-- &#10005: unicode decimal code -->
                        </div>
                    </div>
                    </c:forEach>
                    
                    <div class="back-to-shop"><a href="HomeControl">&leftarrow;</a><span class="text-muted">Back to shop</span></div>
                </div>
                <div class="col-md-4 summary">
                    <div><h5><b>Summary</b></h5></div>
                    <hr>
                    <div class="row">
                        <div class="col" style="padding-left:20px;">ITEMS x ${order.amount}</div>
                        <div class="col text-right">${order.total}00</div>
                    </div>
                    <div class="row" style="border-top: 1px solid rgba(0,0,0,.1); padding: 2vh 0;">
                        <div class="col">TOTAL PRICE</div>
                        <div class="col text-right">${order.total}00</div>
                    </div>
                            <button class="btn"><a href="OrderSubmit">PAYMENT</a></button>
                </div>
            </div>
            
        </div>
        
        <!-- ---------------------------------------------------------------- -->
        <section class="padding"></section>   
        <jsp:include page="Footter.html" />
    </body>
</html>
