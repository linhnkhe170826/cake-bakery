<%-- 
    Document   : Detail
    Created on : Jul 6, 2023, 1:34:30 PM
    Author     : DELL
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="assets/style.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/Cart.js"></script>
<script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="Header.jsp" />
        <section class="padding">
        </section>
        <div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
                                    <c:forEach items="${list}" var="s">
					<div class="col-md-6 col-md-push-1">
						<div id="product-main-img">
							<div class="product-preview">
								<img src="Pic/${s.image}" alt="">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="product-details">
							<h2 class="product-name">${s.name}</h2>
                                                        
                                                        <ul class="product-links">
								<li>Cake Type:</li>
                                                                <li style="font-weight: 600;"><a href="${pageContext.request.contextPath}/TypeControl?numb=${s.typeid}" >${s.typename}</a></li>
							</ul>
                                                        
							<div>
								<h3 class="product-price">${s.price}00</h3>
							</div>
                                                        <p>Thông tin: ${s.infor}</p>

							<div class="add-to-cart">
                                                            <button class="add-to-cart-btn"><a onclick="buy(${s.id})">Add to cart</a></button>
							</div>

							

						</div>
					</div>
                                        </c:forEach>
                                    <div class="col-md-2">
                                        <c:forEach items="${listt}" var="s">
                                                <a href="${pageContext.request.contextPath}/TypeControl?numb=${s.typeid}" ><ul style="opacity: 70%; border-bottom: 2px dashed #D08A64;">${s.typename}</ul></a>
                                        </c:forEach> 
                                    </div>
                                </div>
                        </div>
        </div>
        </
        <jsp:include page="Footter.html" />
    </body>
</html>
