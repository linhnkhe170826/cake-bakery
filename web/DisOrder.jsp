<%-- 
    Document   : DisOrder
    Created on : Jul 8, 2023, 2:08:21 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/board.css">
    </head>
    <body>
        <jsp:include page="Header.jsp" />
   
        <!-------------------------------------------------------------->
        <section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">My Order</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table table-striped">
						  <thead>
						    <tr>
						      <th>ID</th>
						      <th>Total</th>
                                                      <th>Name</th>
                                                      <th>Phone</th>
                                                      <th>Address</th>
						      <th>Items</th>
                                                      <th>Status</th>
                                                      <th>Payment method</th>
                                                      <th>Order date</th>
						    </tr>
						  </thead>
						  <tbody>
                                                      <c:forEach items="${orderLi}" var="li">
                                                        <tr>
                                                          <th scope="row">${li.id}</th>
                                                          <td>${li.total}00</td>
                                                          <td>${li.name}</td>
                                                          <td>${li.phone}</td>
                                                          <td>${li.address}</td>
                                                          <td>
                                                              <c:forEach items="${li.itemAL}" var="it">
                                                                  <p style="font-size: 13px; margin-bottom: -2px;">${it.cname} (${it.price}00) x${it.amount}</p>
                                                              </c:forEach>
                                                          </td>
                                                          <td>
                                                             <c:if test="${li.sattus==1}"><a class="btn btn-danger">Not pay</a></c:if> 
                                                             <c:if test="${li.sattus==2}"><a class="btn btn-warning">Have payed</a></c:if>
                                                             <c:if test="${li.sattus==3}"><a class="btn btn-info">Shipping</a></c:if>
                                                             <c:if test="${li.sattus==4}"><a class="btn btn-success">Done</a></c:if> 
                                                          </td>
                                                          <td>
                                                              <c:if test="${li.paytype==1}"><a class="btn btn-danger">Momo</a></c:if> 
                                                              <c:if test="${li.paytype==2}"><a class="btn btn-info">Zalo pay</a></c:if>
                                                              <c:if test="${li.paytype==3}"><a class="btn btn-success">Pay when recived</a></c:if>
                                                          </td>
                                                          <td>${li.date}</td>
                                                        </tr>
                                                      </c:forEach>
						    
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

        <!-------------------------------------------------------------------->
        
        <jsp:include page="Footter.html" />
    </body>
</html>
