<%-- 
    Document   : Header
    Created on : Jul 5, 2023, 9:54:54 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/css/theme.css" rel="stylesheet" />
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 d-block" data-navbar-on-scroll="data-navbar-on-scroll">
        <div class="container">
          <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
            <a class="navbar-brand d-inline-flex" href="HomeControl"><span style="color:#B44F25 " class=" fs-0 fw-bold ms-2" >Panacota</span></a>
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item px-2"><a class="nav-link fw-medium active" aria-current="page" href="HomeControl">Home</a></li>
              <li class="nav-item px-2"><a class="nav-link fw-medium" href="CakeControl">All Cake</a></li>
              
              <c:choose><c:when test="${empty user}">
                      <li class="nav-item px-2"><a class="nav-link fw-medium" href="Login.jsp">Log In</a></li>
                      <li class="nav-item px-2"><a class="nav-link fw-medium" href="Signup.jsp">Sign Up</a></li>
                  </c:when><c:otherwise>
                      <li class="nav-item px-2"><a class="nav-link fw-medium" href="OrderList">Order</a></li>
                      <li class="nav-item px-2"><a class="nav-link fw-medium" href="UserCenter.jsp">User center</a></li>
                      <li class="nav-item px-2"><a class="nav-link fw-medium" href="Logout">Log Out</a></li>
                  </c:otherwise></c:choose>
                      
                      <c:if test="${!empty user && user.role}">
                          <li class="nav-item px-2"><a class="nav-link fw-medium" href="AdFillter">Manager</a></li>
                      </c:if>
            </ul>
            <form class="shopping">
                <a class="text-1000" href="Cart.jsp">
                    <img src="Pic/cartIcon.jpg">
                    <span class ='quantity'>${order.amount}</span>
                </a>
              </form>
          </div>
        </div>
      </nav>
        
      
    </body>
</html>
