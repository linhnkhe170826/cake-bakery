<%-- 
    Document   : AdWellcome
    Created on : Jul 8, 2023, 7:30:04 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        
        <div class="alert-success" style="margin-top: 30px;text-align: center;font-size: 65px;">Wellcome back my dear friend! 
            <p>May your day be filled with sweetness.</p></div>
    </body>
</html>
