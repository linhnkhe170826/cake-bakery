<%-- 
    Document   : PaySuccess
    Created on : Jul 8, 2023, 1:51:51 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="Header.jsp" />
        <section class="padding">
        </section>
            <div class="alert alert-success ">
            <strong>Payment successfull</strong>
            </div>
        <a href="OrderList" style="margin-left: 40px;"><button>Display order</button></a>
        <jsp:include page="Footter.html" />
    </body>
</html>
