/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author DELL
 */
public class Type extends Cake{
    private int typeid;
    private String typename;

    public Type() {
    }

    public Type(int typeid, String typename) {
        this.typeid = typeid;
        this.typename = typename;
    }

    public Type(int typeid, String typename, int id, String name, String image, float price, int stock, String infor) {
        super(id, name, image, price, stock, infor);
        this.typeid = typeid;
        this.typename = typename;
    }
    
    

    public int getTypeid() {
        return typeid;
    }

    public void setTypeid(int typeid) {
        this.typeid = typeid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    

    @Override
    public String toString() {
        return typeid + typename;
    }
    
    
    
}
