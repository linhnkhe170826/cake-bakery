/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Connection.DBHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class CakeDAO {
    Connection con = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        
        public ArrayList<Cake> findAllCake() {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select C_id,C_image,C_name,C_price,C_infor,C_typename  from Cake,Cake_type where Cake.C_typeid = Cake_type.C_typeid";
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake cake = new Cake(rs.getInt(1), rs.getString(2),rs.getString(3), rs.getFloat(4), rs.getString(5),rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
        
        public ArrayList<Cake> findCakebyRetype(int retype) {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select Cake.C_id,C_image,C_name,C_price,C_infor,C_typename  from Cake,Cake_type,Cake_Re where Cake.C_typeid = Cake_type.C_typeid and Cake.C_id = Cake_Re.C_id and Cake_Re.C_Retype = ?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, retype);
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake cake = new Cake(rs.getInt(1), rs.getString(2),rs.getString(3), rs.getFloat(4), rs.getString(5),rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
        
    
    
    public void updateStatus(int id, int status){
        try {
            con = DBHelper.makeConnection();
            String sql = "update C_Order set Status=? where Or_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public void deleteOrder(int id){
        try {
            con = DBHelper.makeConnection();
            String sql = "delete from C_Order where Or_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public void updateCakeRe(int id, int cakere_id){
        try {
            con = DBHelper.makeConnection();
            String sql = "update Cake_Re set C_Retype=? where C_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, cakere_id);
            stm.setInt(2, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public void deleteCake(int id){
        try {
            con = DBHelper.makeConnection();
            String sql = "delete from Cake where C_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
        
        public int getCake_reid() throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select top(1)id from Cake_Re where C_Retype = 3 order by id ";
        stm =con.prepareStatement(sql);
        rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1)+1;
        }
        else return 1;
    }
        
        public int countbanner() throws SQLException{
        con = DBHelper.makeConnection();
        String sql = " select COUNT(*) from Cake_Re where C_Retype=3";
        stm =con.prepareStatement(sql);
        rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1);
        }
        else return 0;
    }
        
        
    
    public void insertCake_re(Cake_Re cake) throws SQLException{
            con = DBHelper.makeConnection();
            int id = getCake_reid();
            String sql = "Insert into Cake_Re(id,C_Retype,C_id) values (?,?,?)";
            stm = con.prepareStatement(sql);
            stm.setFloat(1, id);
            stm.setInt(2, cake.getCake_Retype());
            stm.setInt(3, cake.getId());
            stm.executeUpdate();
    }
    
    public void deleteCake_Re(int id){
        try {
            con = DBHelper.makeConnection();
            String sql = "delete from Cake_Re where C_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public boolean isRe(Cake cake, int typere){
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Cake_Re where C_Retype=? and C_id =?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, typere);
            stm.setInt(2, cake.getId());
            rs = stm.executeQuery();
            return rs.next();
        }
        catch (SQLException e) { 
        }
        return false;
    }
    
    public int getRetypeId(){
        con = DBHelper.makeConnection();
        String sql = "Select top(1)id from Cake_Re order by id desc";
        try {
            stm =con.prepareStatement(sql);
            rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1)+1;
        }
        else return 1;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }
    
    public void addRetype(int id, int type){
        con = DBHelper.makeConnection();
            int typeid = getRetypeId();
            String sql = "insert into Cake_Re(C_Retype,C_id,id) values (?,?,?)";
        try {
            stm = con.prepareStatement(sql);
            stm.setInt(1, type);
            stm.setInt(2, id);
            stm.setInt(3, typeid);
            stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
            
    }
    
    public void removeRetype(int id, int type){
        con = DBHelper.makeConnection();
            int typeid = getRetypeId();
            String sql = "delete from Cake_Re where C_Retype =? and C_id=?";
        try {
            stm = con.prepareStatement(sql);
            stm.setInt(1, type);
            stm.setInt(2, id);
            stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
            
    }
    
    
    
}
