/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Connection.DBHelper;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author DELL
 */
public class AdDAO implements Serializable{
    Connection con = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        
        public ArrayList<Order> findAllOrder() {
        ArrayList<Order> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select *  from C_Order";
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Order or = new Order(rs.getInt(1), rs.getFloat(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getDate(9));
                list.add(or);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
        
    
    public ArrayList<Order> findOrderbySt(int Status) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select *  from C_Order where Status = ?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, Status);
            rs = stm.executeQuery();
            while (rs.next()) {
                Order or = new Order(rs.getInt(1), rs.getFloat(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getDate(9));
                list.add(or);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public void updateStatus(int id, int status){
        try {
            con = DBHelper.makeConnection();
            String sql = "update C_Order set Status=? where Or_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public void deleteOrder(int id){
        try {
            con = DBHelper.makeConnection();
            String sql = "delete from C_Order where Or_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public void deleteItiem(int id){
        try {
            con = DBHelper.makeConnection();
            String sql = "delete from Item where Or_id=?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
//    ------------------------------------------------------------------
    
    public ArrayList<User> findAllUsers() {
        ArrayList<User> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Accounts";
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getString(6));
                list.add(u);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public void updateUser(String phone, String username, boolean role){
        try {
            con = DBHelper.makeConnection();
            String sql = "update Accounts set Username = ?, Role=? where Phone = ?";
            stm = con.prepareStatement(sql);
            stm.setString(1, username);
            stm.setBoolean(3, role);
            stm.setString(4, phone);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    
    public void updatePass(String phone, String password){
        try {
            con = DBHelper.makeConnection();
            String sql = "update Accounts set Password=? where Phone = ?";
            stm = con.prepareStatement(sql);
            stm.setString(1, password);
            stm.setString(2, phone);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public void addUser(User u) {
        try {
            con = DBHelper.makeConnection();
            String sql = "Insert into Accounts(Username,Password,U_name,Phone,Role,Address,Isvalid) values (?,?,?,?,?,?,?)";
            stm = con.prepareStatement(sql);
            stm.setString(1, u.getUsername());
            stm.setString(2, u.getPassword());
            stm.setString(3, u.getFullname());
            stm.setString(4, u.getPhone());
            stm.setBoolean(5, u.isRole());
            stm.setString(6, u.getAddress());
            stm.setBoolean(7, u.isIsvalid());
            rs = stm.executeQuery();
        }
        catch (SQLException e) { 
        }
    }
    
    public boolean isUserExsit(String username) throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select * from Accounts where Username = ?";
        stm = con.prepareStatement(sql);
        stm.setString(1, username);
        rs = stm.executeQuery();
        if(rs.next()){
            return true; //ton tai tra ve true
        }
        else return false;
    }
    
    public boolean isPhoneExsit(String phone) throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select * from Accounts where Phone = ?";
        stm = con.prepareStatement(sql);
        stm.setString(1, phone);
        rs = stm.executeQuery();
        if(rs.next()){
            return true; //ton tai tra ve true
        }
        else return false;
    }
    
    public boolean phoneIsNumber(String phone){
        for(int i=0;i<phone.length();i++){
                    Character Number = phone.trim().charAt(i);
                    if(Character.isLetter(Number)){
                        return true;
                    }
                }
        return false;
    }
    
    public boolean signupcheck(User user){
        try{
            if(isUserExsit(user.getUsername())){
                return false;
            }
            if(isPhoneExsit(user.getPhone())){
                return false;
            }
            if(phoneIsNumber(user.getPhone())){
                return false;
            }
            if(!user.getPassword().equals(user.getCopassword())){
                return false;
            }
            addUser(user);
            return true;
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    
    public void deleteUser(String phone){
        try {
            con = DBHelper.makeConnection();
            String sql = "delete from Accounts where Phone=?";
            stm = con.prepareStatement(sql);
            stm.setString(1, phone);
            stm.executeUpdate();
        }
        catch (SQLException e) { 
        }
    }
    
    public void addCake(Cake c, int typeid) {
        try {
            con = DBHelper.makeConnection();
            String sql = "Insert into Cake(C_id, C_image, C_name, C_price, C_infor, C_typeid) values (?,?,?,?,?,?)";
            stm = con.prepareStatement(sql);
            AdDAO dao = new AdDAO();
            stm.setInt(1, dao.getNewCakeid()+1);
            stm.setString(2, c.getImage());
            stm.setString(3, c.getName());
            stm.setFloat(4, c.getPrice());
            stm.setString(5, c.getInfor());
            stm.setInt(6, typeid);
            rs = stm.executeQuery();
        }
        catch (SQLException e) { 
        }
    }
    
     public int getNewCakeid() throws SQLException{
        con = DBHelper.makeConnection();
        String sql = " Select top(1)C_id from Cake order by C_id DESC";
        stm =con.prepareStatement(sql);
        rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1);
        }
        else return 0;
    }
     
    public int getTypeId(String typename) throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "  select C_typeid from Cake_type where C_typename like ? ";
        stm =con.prepareStatement(sql);
        stm.setString(1, typename);
        rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1);
        }
        else return 0;
    }
    
}
