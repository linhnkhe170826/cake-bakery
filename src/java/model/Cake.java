/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author DELL
 */
public class Cake {
    private int id;
    private String name;
    private String image;
    private float price;
    private int stock;
    private String infor;
    private String typename;
    
    private boolean isBanner;
    private boolean isBestseller;
    private boolean isNew;

    public Cake() {
    }

    public Cake(int id, String name, String image, float price, int stock, String infor) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.price = price;
        this.stock = stock;
        this.infor = infor;
    }

    public Cake(int id, String image, String name, float price, String infor, String typename) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.price = price;
        this.infor = infor;
        this.typename = typename;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getInfor() {
        return infor;
    }

    public void setInfor(String infor) {
        this.infor = infor;
    }

    @Override
    public String toString() {
        return id + name + image + price + stock + infor +"\n";
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public boolean isIsBanner() {
        return isBanner;
    }

    public void setIsBanner(boolean isBanner) {
        this.isBanner = isBanner;
    }

    public boolean isIsBestseller() {
        return isBestseller;
    }

    public void setIsBestseller(boolean isBestseller) {
        this.isBestseller = isBestseller;
    }

    public boolean isIsNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }



    
    
    
}
