/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Connection.DBHelper;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;

/**
 *
 * @author DELL
 */
public class OrderDAO implements Serializable{
    Connection con = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
    
    public int getOrderid() throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select top(1)Or_id from C_Order order by Or_id DESC";
        stm =con.prepareStatement(sql);
        rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1)+1;
        }
        else return 1;
    }
    
    public int getNewOrderid() throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select top(1)Or_id from C_Order order by Or_id DESC";
        stm =con.prepareStatement(sql);
        rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1);
        }
        else return 0;
    }
    
    public void insertOrder(Order order) throws SQLException{
            con = DBHelper.makeConnection();
            int O_id = getOrderid();
            String sql = "Insert into C_Order(Total,Amount,Status,Paytype,Name,Phone,Address,Or_id, Or_date) values (?,?,?,?,?,?,?,?,?)";
            stm = con.prepareStatement(sql);
            stm.setFloat(1, order.getTotal());
            stm.setInt(2, order.getAmount());
            stm.setInt(3, order.getSattus());
            stm.setInt(4, order.getPaytype());
            stm.setString(5, order.getName());
            stm.setString(6, order.getPhone());
            stm.setString(7, order.getAddress());
            stm.setInt(8, O_id);
            stm.setDate(9, order.getDate());
            stm.executeUpdate();
    }
    
    public int getItiemid() throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select top(1)I_id from Item order by I_id DESC";
        stm =con.prepareStatement(sql);
        rs = stm.executeQuery();
        if(rs.next()){
            return rs.getInt(1)+1;
        }
        else return 1;
    }
    
    public void insertOrderItiem(OrderItem item, int O_id) throws SQLException{
            con = DBHelper.makeConnection();
            int I_id = getItiemid();
            String sql = "Insert into Item(Price,Amount,Or_id,C_id,I_id) values (?,?,?,?,?)";
            stm = con.prepareStatement(sql);
            stm.setFloat(1, item.getPrice());
            stm.setInt(2, item.getAmount());
            stm.setInt(3, O_id);
            stm.setInt(4, item.getCake().getId());
            stm.setInt(5, I_id);
            stm.executeUpdate();  
    }
    
    public void addItem(int Or_id, Order o) throws SQLException{
        OrderDAO dao = new OrderDAO();
        for(OrderItem item : o.getItemMap().values()){
            item.setId(dao.getItiemid());
            dao.insertOrderItiem(item,Or_id);
            }
    }
    
    public ArrayList<Order> getAllOrder(String phone) throws SQLException{
        ArrayList<Order> order = new ArrayList<>();
        con = DBHelper.makeConnection();
        String sql = " select * from C_Order where Phone=? order by Or_date DESC";
        stm =con.prepareStatement(sql);
        stm.setString(1, phone);
        rs = stm.executeQuery();
        while(rs.next()){
            Order o = new Order(rs.getInt(1), rs.getFloat(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getDate(9));
            order.add(o);
        }
        return order;
    }
    
    public ArrayList<OrderItem> getAllItem(int Or_id) throws SQLException{
        ArrayList<OrderItem> item = new ArrayList<>();
        con = DBHelper.makeConnection();
        String sql = "select Item.I_id, Item.Price, Item.Amount, Cake.C_name from Item,C_Order,Cake where C_Order.Or_id = Item.Or_id and Item.C_id = Cake.C_id and C_Order.Or_id = ? ";
        stm =con.prepareStatement(sql);
        stm.setInt(1, Or_id);
        rs = stm.executeQuery();
        while(rs.next()){
            OrderItem i = new OrderItem(rs.getInt(1), rs.getFloat(2), rs.getInt(3), rs.getString(4));
            item.add(i);
        }
        return item;
    } 
    
//    public static void main(String[] args) throws ClassNotFoundException, SQLException {
//        OrderDAO dao = new OrderDAO();
//        Cake c = new Cake(1, "x", "x", 35, 1, "i");
//        Order o = new Order(1, 0, 0, 0, "", "", "");
//        OrderItem i = new OrderItem(2, o , 1, 1, c);
//        dao.insertOrderItiem(i);  
//    }
}
