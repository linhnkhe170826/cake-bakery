/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author DELL
 */
public class Cake_Re extends Cake{
    private int Cake_Retype;

    public Cake_Re() {
    }

    public Cake_Re(int Cake_Retype, int id, String name, String image, float price, int stock, String infor) {
        super(id, name, image, price, stock, infor);
        this.Cake_Retype = Cake_Retype;
    }

    public int getCake_Retype() {
        return Cake_Retype;
    }

    public void setCake_Retype(int Cake_Retype) {
        this.Cake_Retype = Cake_Retype;
    }

    @Override
    public String toString() {
        return Cake_Retype +" "+ super.getId() +" "+ super.getName();
    }

    
    
}
