/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Connection.DBHelper;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author DELL
 */
public class UserDAO implements Serializable{
    Connection con = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        
    public void addUser(User u) {
        try {
            con = DBHelper.makeConnection();
            String sql = "Insert into Accounts(Username,Password,U_name,Phone,Role,Address,Isvalid) values (?,?,?,?,?,?,?)";
            stm = con.prepareStatement(sql);
            stm.setString(1, u.getUsername());
            stm.setString(2, u.getPassword());
            stm.setString(3, u.getFullname());
            stm.setString(4, u.getPhone());
            stm.setBoolean(5, u.isRole());
            stm.setString(6, u.getAddress());
            stm.setBoolean(7, u.isIsvalid());
            rs = stm.executeQuery();
            
        }
        catch (SQLException e) { 
        }
    }
    
    public boolean isUserExsit(String username) throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select * from Accounts where Username = ?";
        stm = con.prepareStatement(sql);
        stm.setString(1, username);
        rs = stm.executeQuery();
        if(rs.next()){
            return true; //ton tai tra ve true
        }
        else return false;
    }
    
    public boolean isPhoneExsit(String phone) throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "Select * from Accounts where Phone = ?";
        stm = con.prepareStatement(sql);
        stm.setString(1, phone);
        rs = stm.executeQuery();
        if(rs.next()){
            return true; //ton tai tra ve true
        }
        else return false;
    }
    
    public boolean phoneIsNumber(String phone){
        for(int i=0;i<phone.length();i++){
                    Character Number = phone.trim().charAt(i);
                    if(Character.isLetter(Number)){
                        return true;
                    }
                }
        return false;
    }
    
    //////////////////////////////////////////////////////
    public boolean signupcheck(User user){
        try{
            if(isUserExsit(user.getUsername())){
                return false;
            }
            if(isPhoneExsit(user.getPhone())){
                return false;
            }
            if(phoneIsNumber(user.getPhone())){
                return false;
            }
            if(!user.getPassword().equals(user.getCopassword())){
                return false;
            }
            addUser(user);
            return true;
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    /////////////////////////////////////////////////////////
    
    public User loginByUsername(String up, String password) throws SQLException{
        con = DBHelper.makeConnection();
                String sql = "Select * from Accounts where Username = ? and Password = ?";
                stm = con.prepareStatement(sql);
                stm.setString(1, up);
                stm.setString(2, password);
                rs = stm.executeQuery();
                if(rs.next()){
                    User u = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getString(6), rs.getBoolean(7));
                    return u;
                }
                return null;
    }
    
    public User loginByPhone(String up, String password) throws SQLException{
            con = DBHelper.makeConnection();
                String sql = "Select * from Accounts where Phone = ? and Password = ?";
                stm = con.prepareStatement(sql);
                stm.setString(1, up);
                stm.setString(2, password);
                rs = stm.executeQuery();
                if(rs.next()){
                    User u = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getString(6), rs.getBoolean(7));
                    return u;
                }
                return null;
    }
    
    public User logincheck(String up, String password) throws SQLException{
        if(loginByUsername(up, password)!=null){
            return loginByUsername(up, password);
        }
        if(loginByPhone(up, password)!=null){
            return loginByPhone(up, password);
        }
        return null;
    }
    
    public void updateUser(User user) throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "update Accounts set U_name = ?, Username = ?, Address = ? where Phone = ?";
        stm = con.prepareStatement(sql);
        stm.setString(1, user.getFullname());
        stm.setString(2, user.getUsername());
        stm.setString(3, user.getAddress());
        stm.setString(4, user.getPhone());
        stm.executeUpdate();
    }
    
    
    public void updatePass(User user) throws SQLException{
        con = DBHelper.makeConnection();
        String sql = "update Accounts set Password = ? where Phone = ?";
        stm = con.prepareStatement(sql);
        stm.setString(1, user.getPassword());
        stm.setString(2, user.getPhone());
        stm.executeUpdate();
    }
    
    
    
    
    public static void main(String[] args) throws SQLException {
        UserDAO dao = new UserDAO();
        //boolean t = dao.logincheck("0813065257", "C123456789");
        User user = new User("12345", "12345", "Ha Noi", "12345");
        dao.updateUser(user);
        //System.out.println(t);
    }
}
