/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author DELL
 */
public class OrderItem {
    private int id;
    private Order order;
    private float price;
    private int amount;
    private String Cname;
    private Cake cake;
    
    public OrderItem() {
    }
    
    
    
    public OrderItem(Order order, float price, int amount, Cake cake) {
        this.order = order;
        this.price = price;
        this.amount = amount;
        this.cake = cake;
    }

    public OrderItem(int id, float price, int amount, String Cname) {
        this.id = id;
        this.price = price;
        this.amount = amount;
        this.Cname = Cname;
    }


    public OrderItem(int id, Order order, float price, int amount, Cake cake) {
        this.id = id;
        this.order = order;
        this.price = price;
        this.amount = amount;
        this.cake = cake;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Cake getCake() {
        return cake;
    }

    public void setCake(Cake cake) {
        this.cake = cake;
    }

    public String getCname() {
        return Cname;
    }

    public void setCname(String Cname) {
        this.Cname = Cname;
    }

    
    
    
}
