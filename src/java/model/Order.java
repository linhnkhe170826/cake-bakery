/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;


import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author DELL
 */
public class Order {
    private int id;
    private float total;
    private int amount;
    private int sattus;
    private int paytype;
    private String name;
    private String phone;
    private String address;
    private Date date;
    private User user;
    private Map<Integer,OrderItem> itemMap = new HashMap<Integer,OrderItem>(); //key is cake id, value is order item
    private ArrayList<OrderItem> itemAL = new ArrayList<>();
    
    public void addCake(Cake c){
        if(itemMap.containsKey(c.getId())){
            OrderItem item = itemMap.get(c.getId());
            item.setAmount(item.getAmount()+1);
        }else{
            OrderItem item = new OrderItem(this,c.getPrice(),1,c); //this order
            itemMap.put(c.getId(), item);
        }
        amount++;
        total+=c.getPrice();
    }
    
    public void lessCake(int cakeid){
        if(itemMap.containsKey(cakeid)){
            OrderItem item = itemMap.get(cakeid);
            item.setAmount(item.getAmount()-1);
            amount--;
            total-=item.getPrice();
            if(item.getAmount()<=0){
                itemMap.remove(cakeid);
            }
        }
    }
    
    public void deleteCake(int cakeid) {
        if(itemMap.containsKey(cakeid)){
            OrderItem item = itemMap.get(cakeid);
            amount-=item.getAmount();
            total-=item.getPrice()*item.getAmount();
            item.setAmount(0);
            itemMap.remove(cakeid);
        }
    }
    
    public Order() {
    }
    
    

    public Order(int id, float total, int amount, int sattus, int paytype, String name, String phone, String address, User user) {
        this.id = id;
        this.total = total;
        this.amount = amount;
        this.sattus = sattus;
        this.paytype = paytype;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.user = user;
    }
    
    public Order( float total, int amount, int sattus, int paytype, String name, String phone, String address) {
        this.total = total;
        this.amount = amount;
        this.sattus = sattus;
        this.paytype = paytype;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public Order(int id, float total, int amount, int sattus, int paytype, String name, String phone, String address, Date date) {
        this.id = id;
        this.total = total;
        this.amount = amount;
        this.sattus = sattus;
        this.paytype = paytype;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.date = date;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSattus() {
        return sattus;
    }

    public void setSattus(int sattus) {
        this.sattus = sattus;
    }

    public int getPaytype() {
        return paytype;
    }

    public void setPaytype(int paytype) {
        this.paytype = paytype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Map<Integer, OrderItem> getItemMap() {
        return itemMap;
    }

    public void setItemMap(Map<Integer, OrderItem> itemMap) {
        this.itemMap = itemMap;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<OrderItem> getItemAL() {
        return itemAL;
    }

    public void setItemAL(ArrayList<OrderItem> itemAL) {
        this.itemAL = itemAL;
    }

    

    

   
    
    

    
    
    
    
}
