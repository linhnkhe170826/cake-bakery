/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author DELL
 */
public class User {
    private String username;
    private String password;
    private String Copassword;
    private String fullname;
    private String phone;
    private boolean role;
    private String address;
    private boolean isvalid = false;

    public User() {
    }

    public User(String username, String password, String fullname, String phone, boolean role, String address, boolean isvalid) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.phone = phone;
        this.role = role;
        this.address = address;
        this.isvalid = isvalid;
    }
    
    
    
    public User(String username, String fullname, String address, String Phone){
        this.username = username;
        this.fullname = fullname;
        this.address = address;
        this.phone = Phone;
    }
    
    public User(String password, String Copassword) {
        this.password = password;
        this.Copassword = password;
    }

    public User(String username, String password, String fullname, String phone, boolean role, String address) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.phone = phone;
        this.role = role;
        this.address = address;
    }

    public User(String username, String password, String Copassword, String fullname, String phone, String address) {
        this.username = username;
        this.password = password;
        this.Copassword = Copassword;
        this.fullname = fullname;
        this.phone = phone;
        this.address = address;
    }

    public User(String username, String password, String Copassword, String fullname, String phone, boolean role, String address) {
        this.username = username;
        this.password = password;
        this.Copassword = Copassword;
        this.fullname = fullname;
        this.phone = phone;
        this.role = role;
        this.address = address;
    }
    
    
    
    
    
    public String getCopassword() {
        return Copassword;
    }

    public void setCopassword(String Copassword) {
        this.Copassword = Copassword;
    }
    
    

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isIsvalid() {
        return isvalid;
    }

    public void setIsvalid(boolean isvalid) {
        this.isvalid = isvalid;
    }
    
    
}
