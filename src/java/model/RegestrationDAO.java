/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Connection.DBHelper;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class RegestrationDAO implements Serializable{
    
    Connection con = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        
    public ArrayList<Cake> findAllCake() {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select *  from Cake";
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake cake = new Cake(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
// --------------------------------------------------------------------------------------------
//------------------------------------TypeDAO here----------------------------------------------    
    public ArrayList<Type> findAllType() {
        ArrayList<Type> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Cake_type";
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Type type = new Type(rs.getInt(1), rs.getString(2));
                list.add(type);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public void changeTypeName(String typename, int typeid){
        try{
        con = DBHelper.makeConnection();
        String sql = "update Cake_type set C_typename = ? where C_typeid = ?";
        stm = con.prepareStatement(sql);
        stm.setString(1, typename);
        stm.setInt(2, typeid);
        stm.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }
    
    
//------------------------------------------------------------------------------------------------------    
//------------------------------------------------------------------------------------------------------    


    public ArrayList<Cake_Re> findCakeType(int caketypeid) {
        ArrayList<Cake_Re> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "  select * \n" +
                         "from Cake \n" +
                        "inner join Cake_Re \n" +
                        "on Cake.C_id = Cake_Re.C_id \n" +
                        "where Cake_Re.C_Retype = ? ";
            stm = con.prepareStatement(sql);
            stm.setInt(1, caketypeid);
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake_Re cakere = new Cake_Re(rs.getInt(10),rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
                list.add(cakere);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public ArrayList<Cake> findCakebyName(String name) {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select *  from Cake where C_name like ?";
            stm = con.prepareStatement(sql);
            stm.setString(1, "%"+name+"%");
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake cake = new Cake(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public ArrayList<Cake> OrCakepriceASC() {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Cake order by C_price";
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake cake = new Cake(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public ArrayList<Cake> OrCakepriceDESC() {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Cake order by C_price DESC";
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake cake = new Cake(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public ArrayList<Cake> findCakebyType(int type) {
        ArrayList<Cake> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Cake where C_typeid = ?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, type);
            rs = stm.executeQuery();
            while (rs.next()) {
                Cake cake = new Cake(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public ArrayList<Type> findCakebyID(int id) {
        ArrayList<Type> list = new ArrayList<>();
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Cake,Cake_type where C_id = ? and Cake.C_typeid = Cake_type.C_typeid";
            stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            rs = stm.executeQuery();
            while (rs.next()) {
                Type cake = new Type(rs.getInt(9),rs.getString(10),rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
                list.add(cake);
            }
        }
        catch (SQLException e) { 
        }
        return list;
    }
    
    public Cake getById(int id) {
        Cake c = new Cake();
        try {
            con = DBHelper.makeConnection();
            String sql = "select * from Cake where C_id = ?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            rs = stm.executeQuery();
            while (rs.next()) {
                c = new Cake(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5), rs.getString(6));
            }
        }
        catch (SQLException e) { 
        }
        return c;
    }
    


    public static void main(String[] args) throws ClassNotFoundException {
        RegestrationDAO dao = new RegestrationDAO();
//        List<Type> list = dao.findCakebyID(1);

//        for (Cake o : list) {
//            System.out.println(o);
//        }
        Cake c = dao.getById(1);
        System.out.println(c);
    }
    
    
    
    
}
