/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controll;

import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import static jdk.nashorn.internal.objects.NativeError.getFileName;
import model.AdDAO;
import model.Cake;


@MultipartConfig
/**
 *
 * @author DELL
 */
public class AdCakeAdd extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    
    }
    


     

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("AdCakeAdd.jsp");
        rd.forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            AdDAO dao = new AdDAO();
            String name = request.getParameter("name");
            float price = Float.parseFloat(request.getParameter("price"));
            String infor = request.getParameter("infor");
            //int stock = Integer.parseInt(request.getParameter("stock"));
            int typeid = Integer.parseInt(request.getParameter("Type_id"));
            String realPath = request.getServletContext().getRealPath("");
            Part filePart = request.getPart("image");
            
        // Obtain the file name
        String fileName = getFileName(filePart);

        // Get the input stream of the file
        try (InputStream fileContent = filePart.getInputStream()) {
            // Specify the directory where you want to save the file
            String uploadDirectory = realPath + File.separator + "Pic" + File.separator;


            // Save the file to the specified directory
            File uploadedFile = new File(uploadDirectory + fileName);
            Files.copy(fileContent, uploadedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        Cake c = new Cake(typeid, fileName, name, price, infor, "");
        dao.addCake(c, typeid);
        request.getRequestDispatcher("AdCake").forward(request, response);

    // Helper method to extract file name from Part
    
            
        } catch (NumberFormatException e) {
            request.getRequestDispatcher("AdCakeAdd.jsp").forward(request, response);        }
    }
        private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : partHeader.split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
