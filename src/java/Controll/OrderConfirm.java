/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controll;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import model.OrderDAO;
import model.OrderItem;
import model.User;

/**
 *
 * @author DELL
 */
public class OrderConfirm extends HttpServlet {
    OrderDAO dao = new OrderDAO();
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Order o = (Order) request.getSession().getAttribute("order");
        String name = request.getParameter("Fullname");
        String address = request.getParameter("Address");
        String phone = request.getParameter("phone");
        int paytype = Integer.parseInt(request.getParameter("paytype"));
        o.setName(name);
        o.setAddress(address);
        o.setPaytype(paytype);
        o.setPhone(phone);
        if(paytype==3) {o.setSattus(1);}
        else {o.setSattus(2);}
        
        o.setDate(new Date(System.currentTimeMillis()));
        o.setUser((User)request.getSession().getAttribute("user"));
        try {
            dao.insertOrder(o);
            int O_id = dao.getNewOrderid();
            if(O_id!=0){
            o.setId(O_id);
            dao.addItem(O_id, o);
        }
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        
            request.getSession().removeAttribute("order");
            request.getRequestDispatcher("PaySuccess.jsp").forward(request, response);
        }
    
     

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
