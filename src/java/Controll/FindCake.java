/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controll;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Cake;
import model.RegestrationDAO;

/**
 *
 * @author DELL
 */
public class FindCake extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        RegestrationDAO dao = new RegestrationDAO();
        int numb = Integer.parseInt(request.getParameter("numb"));
        String search = request.getParameter("search");
        int num = Integer.parseInt(request.getParameter("num"));
        
        ArrayList<Cake> list1 = new ArrayList<>();
        
        list1 = dao.findCakebyType(numb);
        if(num==0 && search==null && numb==0){
            request.getRequestDispatcher("CakeControl.jsp").forward(request, response);
        }
        else if(num==11){
            list1 = dao.OrCakepriceASC();
        } else if(num==10){
            list1 = dao.OrCakepriceDESC();
        }
        else if(numb==1 || numb==2 || numb ==3 || numb==4 || numb==5 || numb==6 || numb ==7 || numb==8 || numb==9){
            list1 = dao.findCakebyType(numb);
        }
        else if(search!=null){
        list1 = dao.findCakebyName(search);
        }
        request.setAttribute("list", list1);
        request.getRequestDispatcher("All Cake.jsp").forward(request, response);
    } 
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
