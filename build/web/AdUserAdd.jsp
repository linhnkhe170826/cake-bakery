<%-- 
    Document   : AddUserAdd
    Created on : Jul 9, 2023, 7:13:05 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        
        <c:if test="${!empty user && user.role}">
            <form action="AdUserListAdd" method="POST">
        <table style="margin-top: 50px; margin-left: 50px;">
                    
            <c:if test="${!empty fmsg}">
                    <div class="alert alert-danger">
                    <strong>${fmsg}</strong>
                    </div>
            </c:if>
            <thead>
                <tr>
                    <td>Fullname: </td>
                    <td> <input type="text" class="input" placeholder="Họ tên" name="Fullname" id="" style="width: 500px;" ></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Phone number: </td>
                    <td><input type="text" class="input" placeholder="Số điện thoại" name="Phone" id="" required="required" style="width: 500px;"></td>
                </tr>
                <tr>
                    <td>Username: </td>
                    <td><input type="text" class="input" placeholder="Tên đăng nhập" name="Username" id="" required="required" style="width: 500px;"></td>
                </tr>
                <tr>
                    <td>Password: </td>
                    <td><input type="Password" class="input" placeholder="Mật khẩu" name="Password" id="" required="required" style="width: 500px;"></td>
                </tr>
                <tr>
                    <td>Comfirm password: </td>
                    <td><input type="Password" class="input" placeholder="Nhập lại mật khẩu" name="CoPassword" id="" required="required" style="width: 500px;"></td>
                </tr>
                <tr>
                    <td>Address: </td>
                    <td><input type="text" class="input" placeholder="Địa chỉ" name="Address" id="" style="width: 500px;"></td>
                </tr>
                <tr>
                    <td>Role: </td>
                    <td><span>Admin <input type="radio" name="role" value="true" /></span>    
                        <span>Normal <input type="radio" name="role" value="false" /></span></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="bt" value="Save" /><td>
                </tr>
            </tbody>
        </table>
                    </form>
        </c:if>         
        
    </body>
</html>
