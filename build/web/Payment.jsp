<%-- 
    Document   : Payment
    Created on : Jul 7, 2023, 7:52:55 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/css/payment.css" rel="stylesheet" />
        <script type="text/javascript" src="assets/js/Cart.js"></script>
        <script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
    </head>
    <body>
        <jsp:include page="Header.jsp" />
        <section class="padding">
        </section>
                
                
                <div class="container">
                    <form action="OrderConfirm" method="POST">
                        <div class="top">
                            <h5 class="fw-bold fs-3 fs-lg-5 lh-sm mb-3" style="color: #824a03; padding-top: 20px;">Payment Detail</h5>
                        </div>
                        <div style="color: #9a5a09; font-size: 30px; padding-bottom: 10px;text-align: left;">User Information</div>
                        <div class="smalltitle">
                        <table >
                            <thead>
                                <tr> 
                                    <td>Full Name:</td>
                                    <td><input type="text" class="input" name="Fullname" value="${user.fullname}" ></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Phone:</td>
                                    <td><input name="phone" value="${user.phone}" readonly></td>
                                </tr>
                                <tr>
                                    <td>Address:</td>
                                    <td><input type="text" class="input" name="Address" value="${user.address}" required="required"></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>

                            <!-- ----------------------------------------------------------------------- -->         
                            <div style="color: #9a5a09; font-size: 30px; padding-bottom: 10px;text-align: left;">Choose payment method</div>
                            <div style="color: #000; font-size: 20px; padding-bottom: 10px;text-align: left;">Total money: ${order.total}00</div>
                            <div class="row">
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <label>
                                    <div class="Payment">
                                        <input type="radio" name="paytype" value="1" checked="checked"/>
                                        <img src="Pic/pay1.png" alt="momo">
                                    </div> 
                                </label>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <label>
                                    <div class="Payment">
                                        <input type="radio" name="paytype" value="2"/>
                                        <img src="Pic/pay2.png" alt="zalo">
                                    </div> 
                                </label>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <label>
                                    <div class="Payment">
                                        <input type="radio" name="paytype" value="3"/>
                                        <img src="Pic/pay3.png" alt="cod">
                                    </div> 
                                </label>
                            </div>
                        </div>
                            <button class="add-to-cart-btn"><input type="submit" value="Submit"></button>
                    </form>           
                </div>
                        
            
                        
        
        <jsp:include page="Footter.html" />
    </body>
</html>
