<%-- 
    Document   : AdUserFix
    Created on : Jul 9, 2023, 5:57:20 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/board.css">
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
    <c:if test="${!empty user && user.role}">
        <form action="AdUserFix" method="POST">
        <table style="margin-top: 50px; margin-left: 30px;">
            <thead>
                <tr>
                    <td>User: </td>
                    <td>${param.username}</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Phone number: </td>
                    <td>${param.phone} <input type="hidden" name="phone" value="${param.phone}"></td>
                </tr>
                <tr>
                    <td>Password: </td>
                    <td><input type="Password" class="input" placeholder="Mật khẩu" name="Password" id="" required="required"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="bt" value="Save" /><td>
                </tr>
            </tbody>
        </table>
                    </form>
    </c:if>
        
        
            
            
    </body>
</html>
