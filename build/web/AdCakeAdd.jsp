<%-- 
    Document   : AdCakeAdd
    Created on : Jul 9, 2023, 10:48:09 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        <c:if test="${!empty user && user.role}">
            <form action="AdCakeAdd" method="POST" enctype="multipart/form-data">
        <table style="margin-top: 50px; margin-left: 50px;">
                    
            <thead>
                <tr>
                    <td>Name: </td>
                    <td> <input type="text" class="input" name="name" id="" style="width: 500px;" required></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Image </td>
                    <td><input type="file" name="image" accept=".png, .jpg, .jpeg, .jfif"></td>
                </tr>
                <tr>
                    <td>Price: </td>
                    <td><input type="text" class="input" name="price" id="" style="width: 500px;" required></td>
                </tr>
                <tr>
                    <td>Description: </td>
                    <td><input type="text" class="input" name="infor" id="" style="width: 500px;"></td>
                </tr>
<!--                <tr>
                    <td>Stock: </td>
                    <td><input type="text" class="input"  name="stock" id="" style="width: 500px;"></td>
                </tr>-->
                <tr>
                    <td>Type: </td>
                    <td> <select class="input-select" name="Type_id">
                            <option value="" disabled selected="">Choose type</option>
                            <c:forEach items="${listt}" var="type">
                                <option value="${type.typeid}">${type.typename}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="bt" value="Save" /><td>
                </tr>
            </tbody>
        </table>
                    </form>
        </c:if>
                    
        
    </body>
</html>
