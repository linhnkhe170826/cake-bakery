<%-- 
    Document   : AdType
    Created on : Jul 9, 2023, 10:31:46 AM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/board.css">
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        <c:if test="${!empty user && user.role}">
            <!-------------------------------------------------------------->
            <section class="ftco-section">
                    <div class="container">
                            <div class="row">
                                    <div class="col-md-12">
                                            <div class="table-wrap">
                                                    <table class="table table-striped">
                                                      <thead>
                                                        <tr>
                                                          <th>ID</th>
                                                          <th>Type name</th>                                                   
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                          <c:forEach items="${typeli}" var="li">
                                                            <tr>
                                                              <td>${li.typeid}</td>
                                                              <td>${li.typename}</td>
                                                            </tr>
                                                          </c:forEach>

                                                      </tbody>
                                                    </table>
                                            </div>
                                    </div>
                            </div>
                    </div>
            </section>
        </c:if>
        
    </body>
</html>
