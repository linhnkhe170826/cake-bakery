<%-- 
    Document   : Login
    Created on : Jul 5, 2023, 9:26:05 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/css/login.css" rel="stylesheet" />
        
    </head>
    
    <body>
        <jsp:include page="Header.jsp" />
        <section class="padding">
        </section>
            
            
        <form action = "Login" method="POST">
            <div class="box" style="background-image:url(Pic/LoginBack.jpg);">
                
                <div class="container">
                    <div class="col-lg-7 mx-auto text-center mb-6">
                        
                    <div class="top">
                        <h5 class="fw-bold fs-3 fs-lg-5 lh-sm mb-3" style="color: #ffffff; padding-top: 20px">Đăng nhập</h5>
                    </div>
                        <c:if test="${!empty msg}">
                        <div class="alert alert-success ">
                        <strong>${msg}</strong>
                        </div>
                        </c:if>
            
            
                        <c:if test="${!empty fmsg}">
                        <div class="alert alert-danger ">
                        <strong>${fmsg}</strong>
                        </div></c:if>
                    <div class="input-field">
                        <input type="text" class="input" placeholder="Username" name="up" id="" required="required">
                        <i class='bx bx-user' ></i>
                    </div>
            
                    <div class="input-field">
                        <input type="Password" class="input" placeholder="Mật khẩu" name="Password" id="" required="required">
                        <i class='bx bx-lock-alt'></i>
                    </div>
            
                    <div class="input-field">
                        <input type="submit" class="submit" value="Đăng nhập" name="btAction" id="">
                    </div>
            
                    <div class="two-col">
                        <div class="two">
                            <label><a href="Signup.jsp">Đăng kí</a></label>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            


        </form>
    <jsp:include page="Footter.html" />
   
</body>
</html>
