<%-- 
    Document   : AdCakeChangeInfor
    Created on : Jul 9, 2023, 11:29:13 PM
    Author     : DELL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        <c:if test="${!empty user && user.role}">
            <form action="AdCakeChangeInfor" method="POST">
        <table style="margin-top: 50px; margin-left: 50px;">
                    
            <thead>
                <tr>
                    <td>Name: </td>
                    <td> <input type="text" class="input" placeholder="Name" name="name" id="" required="required" style="width: 500px;" ></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Image </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Price: </td>
                    <td><input type="text" class="input" placeholder="Price" name="price" id="" required="required" style="width: 500px;"></td>
                </tr>
                <tr>
                    <td>Description: </td>
                    <td><input type="text" class="input" placeholder="information" name="infor" id="" required="required" style="width: 500px;"></td>
                </tr>
                <tr>
                    <td>Type: </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="bt" value="Save" /><td>
                </tr>
            </tbody>
        </table>
                    </form>
        </c:if>
                    
        
    </body>
</html>
