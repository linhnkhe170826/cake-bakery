<%-- 
    Document   : AdUserFixInfor
    Created on : Jul 9, 2023, 8:50:53 AM
    Author     : DELL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <jsp:include page="AdHeader.jsp"/>
        <br><br>
        <c:if test="${!empty user && user.role}">
            <form action="AdUserFixInfor" method="POST">
        <table style="margin-top: 50px; margin-left: 30px;">
            <thead>
                
                <tr>
                    <td>User:  </td>
                    <td> ${param.username}</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Phone number: </td>
                    <td>${param.phone} <input type="hidden" name="oldphone" value="${param.phone}"></td>
                </tr>
                <tr>
                    <td>Phone number: </td>
                    <td><input type="text" class="input" name="phone" value="${param.phone}" required="required"></td>
                </tr>
                <tr>
                    <td>Username: </td>
                    <td><input type="text" class="input" name="username" value="${param.username}" required="required"></td>
                </tr>
                <tr>
                    <td>Fullname: </td>
                    <td><input type="text" class="input" name="fullname" value="${param.fullname}"></td>
                </tr>
                <tr>
                    <td>Role: </td>
                    <td>
                            <span>Admin <input type="radio" name="role" value="true" checked="ON"/></span>
                            <span>Normal <input type="radio" name="role" value="false" /></span></td>
                        
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="bt" value="Save" /><td>
                </tr>
            </tbody>
        </table>
                    </form>
        </c:if>
        
    </body>
</html>
