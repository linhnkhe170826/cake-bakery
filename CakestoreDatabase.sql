USE [master]
GO
/****** Object:  Database [Cakestore]    Script Date: 11/22/2023 4:52:00 AM ******/
CREATE DATABASE [Cakestore]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Cakestore', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.NKLINH\MSSQL\DATA\Cakestore.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Cakestore_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.NKLINH\MSSQL\DATA\Cakestore_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Cakestore] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Cakestore].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Cakestore] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Cakestore] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Cakestore] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Cakestore] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Cakestore] SET ARITHABORT OFF 
GO
ALTER DATABASE [Cakestore] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Cakestore] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Cakestore] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Cakestore] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Cakestore] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Cakestore] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Cakestore] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Cakestore] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Cakestore] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Cakestore] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Cakestore] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Cakestore] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Cakestore] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Cakestore] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Cakestore] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Cakestore] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Cakestore] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Cakestore] SET RECOVERY FULL 
GO
ALTER DATABASE [Cakestore] SET  MULTI_USER 
GO
ALTER DATABASE [Cakestore] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Cakestore] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Cakestore] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Cakestore] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Cakestore] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Cakestore] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Cakestore', N'ON'
GO
ALTER DATABASE [Cakestore] SET QUERY_STORE = OFF
GO
USE [Cakestore]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 11/22/2023 4:52:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[Username] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
	[U_name] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NOT NULL,
	[Role] [bit] NULL,
	[Address] [nvarchar](100) NULL,
	[Isvalid] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Phone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Order]    Script Date: 11/22/2023 4:52:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Order](
	[Or_id] [int] NOT NULL,
	[Total] [float] NULL,
	[Amount] [int] NULL,
	[Status] [tinyint] NULL,
	[Paytype] [tinyint] NULL,
	[Name] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[Address] [nvarchar](255) NULL,
	[Or_date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[Or_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cake]    Script Date: 11/22/2023 4:52:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cake](
	[C_id] [int] NOT NULL,
	[C_name] [nvarchar](255) NULL,
	[C_image] [nvarchar](255) NULL,
	[C_price] [float] NULL,
	[C_stock] [int] NULL,
	[C_infor] [nvarchar](300) NULL,
	[C_mark] [nvarchar](100) NULL,
	[C_typeid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[C_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cake_Re]    Script Date: 11/22/2023 4:52:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cake_Re](
	[id] [int] NOT NULL,
	[C_Retype] [tinyint] NULL,
	[C_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cake_type]    Script Date: 11/22/2023 4:52:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cake_type](
	[C_typeid] [int] NOT NULL,
	[C_typename] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[C_typeid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 11/22/2023 4:52:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[I_id] [int] NOT NULL,
	[Price] [float] NULL,
	[Amount] [int] NULL,
	[C_id] [int] NULL,
	[Or_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[I_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'Calusxongmao', N'c', N'Calus', N'0813065257', 1, NULL, NULL)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'Nguyễn Linh', N'1', N'Linh', N'0913065257', 0, N'', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'1', N'1', N'1', N'1', 0, N'Ha Noi', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'10', N'10', N'10', N'10', 0, N'10', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'123', N'1234', N'1234', N'1234', 0, N'Ha Noi', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'12345', N'12345', N'12345', N'12345', 0, N'Ha Noi', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'Stellexongmao', N'S123456789', N'Stellexongmao', N'1234567890', 1, N'Ha Noi', NULL)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'Li', N'L123456789', N'Nguyen Li', N'2345678901', 0, N'Ha Noi', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'3', N'12', N'3', N'3', 0, N'Ha Noi', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'DuongLS', N'D123456789', N'Lam Duong', N'3456789012', 0, NULL, NULL)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'6', N'6', N'6', N'6', 0, N'6', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'7', N'7', N'7', N'7', 1, N'7', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'8', N'8', N'8', N'8', 1, N'8', 0)
INSERT [dbo].[Accounts] ([Username], [Password], [U_name], [Phone], [Role], [Address], [Isvalid]) VALUES (N'9', N'9', N'9', N'9', 0, N'9', 0)
GO
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (1, 170, 4, 4, 1, N'1', N'1', N'Ha Noi', CAST(N'2023-07-08' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (2, 305, 7, 3, 2, N'1', N'1', N'Ha Noi', CAST(N'2023-07-08' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (3, 385, 6, 1, 3, N'1', N'1', N'Ha Noi', CAST(N'2023-07-08' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (4, 215, 3, 3, 2, N'1', N'1', N'Ha Noi', CAST(N'2023-07-08' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (5, 260, 10, 3, 2, N'Stelle', N'1234567890', N'Ha Noi', CAST(N'2023-07-08' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (6, 250, 5, 2, 1, N'Stelle', N'1234567890', N'Ha Noi', CAST(N'2023-07-08' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (8, 85, 2, 2, 1, N'Lam Duong', N'3456789012', N'Ha Noi', CAST(N'2023-07-09' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (9, 125, 3, 1, 3, N'7', N'7', N'7', CAST(N'2023-07-10' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (10, 270, 3, 2, 1, N'6', N'6', N'6', CAST(N'2023-07-10' AS Date))
INSERT [dbo].[C_Order] ([Or_id], [Total], [Amount], [Status], [Paytype], [Name], [Phone], [Address], [Or_date]) VALUES (11, 35, 1, 2, 1, N'7', N'7', N'7', CAST(N'2023-09-14' AS Date))
GO
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (1, N'Bánh sừng bò', N'1.jpg', 35, 10, N'Bánh mì bơ ngàn lớp ', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (2, N'Bánh sừng bò hạnh nhân', N'2.jpg', 45, 10, N'Bánh mì bơ ngàn lớp kèm kem hạnh nhân', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (4, N'Bánh Danish nho', N'4.jpg', 45, 10, N'Vỏ bánh ngàn lớp kết hợp cùng nho khô', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (5, N'Bánh Danish Chocolate', N'5.jpg', 45, 10, N'Vỏ bánh ngàn lớp kết hợp chocolate', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (6, N'Bánh Phomai dẻo', N'6.jpg', 95, 10, N'Bánh mì phomai dẻo làm từ bột năng, bơ và các loại phomai hảo hạng', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (7, N'Bánh mì sữa', N'7.jpg', 40, 10, N'Bánh mì sữa mềm', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (8, N'Bánh mì hạnh nhân kem sữa', N'8.jpg', 45, 10, N'Bánh mì mềm nhân kem sữa, phủ hạnh nhân nướng giòn', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (10, N'Bánh mì nho dừa', N'10.jpg', 85, 10, N'Bánh mì mềm kết hợp sợi dừa tươi
 và nho khô', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (11, N'Bánh mì hoa cúc', N'11.jpg', 95, 10, N'Bánh mì bơ mềm kiểu Pháp hương hoa cam', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (12, N'Bánh mì xúc xích phomai', N'12.jpg', 45, 10, N'Bánh mì mềm kết hợp cùng xúc xích hun khói và phomai mozarella', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (13, N'Bánh mì ham cheese ngô nấm', N'13.jpg', 75, 10, N'Bánh mì nhân thịt xông khói, nấm và rau củ quả', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (14, N'Bánh mì phomai chà bông', N'14.jpg', 95, 10, N'Bánh mì mềm cùng sốt phomai và chà bông ngũ vị', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (15, N'Bánh donut nhân kem', N'15.jpg', 115, 10, N'Bánh donut nhân kem mix 4 vị:Kem trứng, trà tranh,cà phê, socola', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (16, N'Bánh mì bơ quế', N'16.jpg', 75, 10, N'Bánh mì mềm cuộn bơ, đường nâu và bột quế', NULL, 1)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (17, N'Bánh con sò', N'17.jpg', 20, 10, N'Bánh bơ nướng hình con sò', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (18, N'Bánh Canele', N'18.jpg', 35, 10, N'Bánh bò nướng kiểu Pháp cùng vani và rượu rum', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (19, N'Bánh Financier nam việt quất', N'19.jpg', 20, 10, N'Bánh hạnh nhân nướng cùng quả nam việt quất', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (20, N'Bánh Financier caramel bơ mặn', N'20.jpg', 20, 10, N'Bánh hạnh nhân nướng cùng sốt caramel bơ mặn', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (21, N'Bánh Financier gấu con', N'21.jpg', 25, 10, N'Bánh hạnh nhân nướng hình gấu con', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (22, N'Bánh Financier donut', N'22.jpg', 40, 10, N'Bánh financier hạnh nhân, phủ socola và kẹo bi đường', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (23, N'Bánh su kem vỏ thường', N'23.jpg', 60, 10, N'Bánh su kem nhân kem trứng sữa', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (24, N'Bánh su kem socola hạnh nhân', N'24.jpg', 85, 10, N'Bánh su kem phủ socola đen cùng hạnh nhân nướng nhân kem trứng sữa', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (25, N'Bánh xu kem vỏ xù ', N'25.jpg', 70, 10, N'Bánh su kem vỏ xù nhân kem trứng sữa', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (26, N'Bánh muffin mix 4 vị', N'26.jpg', 95, 10, N'Bánh nướng xốp mix 4 vị:vanilla nho,chocolate,cherry,cafe', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (28, N'Bánh bông lan phomai nhật', N'28.jpg', 155, 10, N'Bánh bông lan phomai kiểu Nhật', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (29, N'Bánh bông lan socola tươi', N'29.jpg', 95, 10, N'Bánh bông lan socola tươi nướng', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (30, N'Bánh bông lan trứng muối ', N'30.jpg', 270, 10, N'Cốt bánh bông lan kết hợp cùng sốt phomai, sốt kem trứng, trứng muối và chà bông', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (31, N'Bánh bông lan chà bông rong biển', N'31.jpg', 65, 10, N'Bánh bông lan kèm sốt kem trứng và chà bông rong biển', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (32, N'Bánh brownie', N'32.jpg', 55, 10, N'
Bánh socola đen cùng hạt hạnh nhân và óc chó', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (33, N'Bánh quiche lorraine', N'33.jpg', 45, 10, N'Bánh tart mặn phomai thịt hun khói', NULL, 2)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (34, N'Bánh Flan(set 8 hộp)', N'34.jpg', 120, 10, N'Bánh flan trứng sữa mềm mịn', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (35, N'Panna cotta', N'35.jpg', 35, 10, N'Kem sữa béo nấu đông ăn kèm sốt trái cây', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (36, N'Tiramisu', N'36.jpg', 135, 10, N'Bánh kem trứng sữa kết hợp với bánh sampa tẩm cafe và rượu rum', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (37, N'Pudding chocolate', N'37.jpg', 50, 10, N'Bánh kem sữa socola đen đắng
', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (38, N'Mousse chanh leo', N'38.jpg', 50, 10, N'Bánh kem lạnh vị chanh leo', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (39, N'Mousse phúc bồn tử ', N'39.jpg', 50, 10, N'Bánh kem lạnh vị phúc bồn tử', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (40, N'Mousse xoài mơ', N'40.jpg', 50, 10, N'Bánh kem lạnh vị xoài mơ', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (41, N'Oreo cheese cake', N'41.jpg', 50, 10, N'Bánh phomai tươi kết hợp cùng bánh oreo nghiền', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (42, N'Nutella cheese cake', N'42.jpg', 50, 10, N'Bánh phomai kem lạnh mứt hạt dẻ', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (43, N'Bánh phomai 3 lớp(set 2 bánh)', N'43.jpg', 85, 10, N'Bánh phomai đặc biệt với lớp phomai nướng, lớp phomai kem lạnh và đế gato bông xốp', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (44, N'Bánh phomai cháy(set 2 bánh)', N'44.jpg', 85, 10, N'Bánh phomai nướng với mặt bánh cháy đậm vị caramel kết hợp với phần kem phomai tươi chua nhẹ thơm béo', NULL, 3)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (45, N'Bánh gato Phomai xoài', N'45.jpg', 135, 10, N'Gato kem tươi phomai kết hợp vớixoài tươi và mứt xoài chua ngọt', NULL, 4)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (46, N'Bánh gato Caramel bơ mặn', N'46.jpg', 135, 10, N'Gato kem tươi caramel kết hợp cùng sốt caramel bơ mặn', NULL, 4)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (47, N'Bánh gato trà xanh gạo rang', N'47.jpg', 135, 10, N'Gato kem tươi kết hợp với sốt trà xanh gạo rang Nhật', NULL, 4)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (48, N'Bánh gato kem trứng dừa nướng ', N'48.jpg', 105, 10, N'
Gato kem trứng kết hợp với vụn dừa nướng giòn', NULL, 4)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (49, N'Bánh gato socola cafe', N'49.jpg', 150, 10, N'Gato socola kem cafe kết hợp cùng sốt socola đen đắng', NULL, 4)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (50, N'Bánh Red Velvet', N'50.jpg', 150, 10, N'Gato socola nhung đỏ kết hợp với kem bơ phomai', NULL, 4)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (51, N'Cookie bơ hạnh nhân 250gr', N'51.jpg', 140, 10, N'Bánh quy bơ hạnh nhân giòn xốp', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (52, N'Cookie bơ  trà xanh 250gr', N'52.jpg', 140, 10, N'Bánh quy bơ trà xanh giòn xốp', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (53, N'Cookie bơ vani socola 250gr', N'53.jpg', 140, 10, N'Bánh quy bơ vani và socola kết hợp', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (54, N'Cookie bơ socola chip 250gr', N'54.jpg', 140, 10, N'Bánh quy bơ kết hợp cùng hạt socola chip đen đắng', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (55, N'Cookie bơ socola hạnh nhân 250gr', N'55.jpg', 140, 10, N'Bánh quy bơ socola đen kết hợp cùng hạnh nhân lát', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (56, N'Cookie bơ phomai hun khói 250gr', N'56.jpg', 140, 10, N'
Bánh quy bơ mặn kết hợp cùng phomai hun khói và lá ngò tây', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (57, N'Bánh Biscotti 250gr', N'57.jpg', 140, 10, N'Bánh quy thái lát kết hợp cùng quả khô dinh dưỡng', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (58, N'Bánh ngói hạnh nhân 250gr', N'58.jpg', 140, 10, N'Bánh quy bơ hạnh nhân lát nướng giòn', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (59, N'Bánh hạt quả khô 500gr', N'59.jpg', 300, 10, N'Bánh quy hạt quả khô dinh dưỡng, không chứa tinh bột', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (60, N'Ngũ cốc granolla mật ong 500gr', N'60.jpg', 225, 10, N'Các loại quả khô dinh dưỡng tẩm mật  nướng giòn', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (61, N'Choco nut cookie', N'61.jpg', 25, 10, N'Bánh quy mềm socola đen đắng cùng các loại hạt ', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (62, N'Choco levain cookie', N'62.jpg', 25, 10, N'Bánh quy mềm đường nâu cùng socola đen và hạt óc chó', NULL, 5)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (63, N'Socola tươi vị cacao (Hu 12 viên)', N'63.jpg', 180, 10, N'Truffle chocolate', NULL, 6)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (64, N'Socola tuoi mix 5 vị (Set 18 viên)', N'64.jpg', 180, 10, N'Socola mix 5 vị:cacao/matcha/coconut/oreo/almond', NULL, 6)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (65, N'Sữa chua kem tuoi (Set 6 hộp)', N'65.jpg', 90, 10, N'Sữa chua tươi được làm theo công thức riêng', NULL, 7)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (66, N'Sữa chua Hy Lạp', N'66.jpg', 45, 10, N'Sữa chua Hi Lạp sánh đặc và béo ngậy được làm theo công thức riêng', NULL, 7)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (67, N'Chè khúc bạch long nhãn', N'67.jpg', 35, 10, N'', NULL, 8)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (68, N'Chè dừa non thốt nốt', N'68.jpg', 35, 10, N'', NULL, 8)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (69, N'Thạch sữa ngô lá nếp', N'69.jpg', 55, 10, N'', NULL, 8)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (70, N'Sữa chua thạch lá nếp', N'70.jpg', 30, 10, N'', NULL, 8)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (71, N'Thạch cafe flan phomai', N'71.jpg', 55, 10, N'', NULL, 8)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (72, N'Set Tea time', N'72.jpg', 240, 10, N'Gồm 8 loại bánh lạnh mini size: moussechanh leo/ việt quất, tiramisu truyền thống/ trà xanh, cheesecake oreo/ nutella, pudding xoài mơ/chocolate', NULL, 9)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (73, N'Set Merry', N'73.jpg', 380, 10, N'Gồm 3 loại bánh nướng kiểu Pháp mini size: bánh con sò, bánh financier caramel bơ mặn, bánh financier hạt dẻ cười', NULL, 9)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (74, N'Set Happy', N'74.jpg', 250, 10, N'Gồm 7 loại bánh nướng đặc trưng: bánh castella socola tươi, su kem, bánh con sò, bánh financier caramel bơ mặn, bánh financier hạt dẻ cười, cookie trà xanh, cookie socola hạnh nhân', NULL, 9)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (75, N'Set Party', N'75.jpg', 450, 10, N'Gồm 10 loại bánh nướng đặc trưng: bánh phomai dẻo, bánh bông lan trứng muối, bánh su kem phủ socola hạnh nhân, bánh con sò, bánh financier hạt dẻ cười, bánh financier caramel bơ mặn, bánh cookie marble, cookie trà xanh, cookie socola hạnh nhân, cookie phomai hun khói', NULL, 9)
INSERT [dbo].[Cake] ([C_id], [C_name], [C_image], [C_price], [C_stock], [C_infor], [C_mark], [C_typeid]) VALUES (76, N'Croll box', N'76.jpg', 230, 10, N'Gồm 6 loại bánh bột ngàn lớp: bánh sừng bò, bánh sừng bò hạnh nhân, bánh tart trứng, bánh pateso, bánh danish chocolate, bánh danish nho', NULL, 9)
GO
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (1, 1, 15)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (2, 1, 22)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (3, 1, 2)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (4, 1, 44)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (5, 2, 35)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (6, 2, 36)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (7, 2, 39)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (8, 2, 41)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (9, 3, 1)
INSERT [dbo].[Cake_Re] ([id], [C_Retype], [C_id]) VALUES (10, 3, 38)
GO
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (1, N'Bánh mì')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (2, N'Bánh lạnh')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (3, N'Bánh nướng')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (4, N'Cake box')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (5, N'Cookie')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (6, N'Chocolate')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (7, N'Sữa chua')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (8, N'Chè Thạch')
INSERT [dbo].[Cake_type] ([C_typeid], [C_typename]) VALUES (9, N'Set bánh')
GO
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (1, 35, 1, 1, 1)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (2, 45, 1, 2, 1)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (3, 50, 1, 38, 1)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (4, 40, 1, 22, 1)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (5, 35, 1, 1, 2)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (6, 45, 1, 2, 2)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (7, 45, 2, 4, 2)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (8, 45, 3, 5, 2)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (9, 55, 1, 32, 3)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (10, 45, 1, 33, 3)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (11, 135, 1, 36, 3)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (12, 50, 2, 37, 3)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (13, 50, 1, 41, 3)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (14, 45, 1, 12, 4)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (15, 75, 1, 13, 4)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (16, 95, 1, 14, 4)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (17, 20, 1, 17, 5)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (18, 35, 3, 1, 5)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (19, 20, 2, 19, 5)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (20, 20, 1, 20, 5)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (21, 25, 1, 21, 5)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (22, 25, 1, 61, 5)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (23, 25, 1, 62, 5)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (24, 50, 5, 38, 6)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (26, 35, 1, 1, 8)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (27, 50, 1, 38, 8)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (28, 35, 1, 1, 9)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (29, 40, 1, 22, 9)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (30, 50, 1, 39, 9)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (31, 40, 1, 22, 10)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (32, 115, 2, 15, 10)
INSERT [dbo].[Item] ([I_id], [Price], [Amount], [C_id], [Or_id]) VALUES (33, 35, 1, 1, 11)
GO
ALTER TABLE [dbo].[C_Order]  WITH CHECK ADD  CONSTRAINT [number] FOREIGN KEY([Phone])
REFERENCES [dbo].[Accounts] ([Phone])
GO
ALTER TABLE [dbo].[C_Order] CHECK CONSTRAINT [number]
GO
ALTER TABLE [dbo].[Cake]  WITH CHECK ADD FOREIGN KEY([C_typeid])
REFERENCES [dbo].[Cake_type] ([C_typeid])
GO
ALTER TABLE [dbo].[Cake_Re]  WITH CHECK ADD FOREIGN KEY([C_id])
REFERENCES [dbo].[Cake] ([C_id])
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [cak] FOREIGN KEY([C_id])
REFERENCES [dbo].[Cake] ([C_id])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [cak]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [orde] FOREIGN KEY([Or_id])
REFERENCES [dbo].[C_Order] ([Or_id])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [orde]
GO
USE [master]
GO
ALTER DATABASE [Cakestore] SET  READ_WRITE 
GO
