# Cake Bakery



## Getting started

Hello, this is my cake bakery webapp project, an assignment for Java Web Application Development (PRJ301). In this project, I play the role of a developer, designing a webapp according to my understanding of a website selling cakes. I did this project alone for two weeks.

## Tools

- Apache NetBeans IDE 13

## Name
Panacota cake bakery webapp

## Description
This webapp help custumer can order cake from the bakery and track the order. This webapp have two main role, customer and admin. They all can login into the sytem, view all cake, sort, search and add cake into cart. When user login, they can make an order. Customer can track their order status in my order page. Admin can manage order like delete or change order status, admin account, admin cake and see all cake type. In manage account, admin can add new account, change account information, password or delete it. In manage cake, admin can add new cake, delete cake, add it into banner section, best seller section or new product section.
Because time is limited, so this webapp just contain basic function, many functions are still out of scope like check customers address, create account by gmail, link payment into pay app.


